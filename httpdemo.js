/*
jshint node: true
*/
/*global require:true*/
var express = require('express');
var bodyParser  = require('body-parser');

var _ = require('underscore');


var app = express();


//obiekt gry
var game = {
    maxMoves: 0,
    movesLeft: 0,
    maxColors: 0,
    codeSize: 5,
    secret: []
    
};



//functions
var mark = function (key, guess) {
    console.log(guess);
    var tabblack = _.map(key, function (el, idx) {
        if (el === guess[idx]) {
            el = "black";
            guess[idx] = "black";
            return el;
        } else {return el; }
    });
    var blacknum =  _.filter(tabblack, function(num){ return num === "black"; }).length;
    
    var tabwhite = _.map(tabblack, function (el, idx) {
            if (_.contains(tabblack,guess[idx]) && guess[idx] !== "black" ) {
                return "white";
            } else return el;
    });
    var whitenum =  _.filter(tabwhite, function(num){return num === "white";}).length;         
    
    return{ 
        black: blacknum,
        white: whitenum
    };
};

var randomizeSecret = function (size, max) {
    var m = parseInt(max);
    for (var i = 0; i < size ; i++){
        game.secret[i] = Math.floor((Math.random() * m) + 1); 
    }
};


//warstwy



app.use('/js', express.static('bower_components/jquery/dist'));
app.use(express.static('public'));
app.use(bodyParser.json());


app.post('/api/newgame', function (req, res) {
    console.dir(req.body);
    //losujemy nowy kod ( o ile parametry som ok)
    // i modyfikujemy obiekt gry
    randomizeSecret(req.body.codeSize, req.body.maxColors);
    game.maxMoves = req.body.maxMoves;
    game.movesLeft = req.body.maxMoves;
    game.maxColors = req.body.maxColors;
                    
    console.dir(game);
    
    res.end();
    
    
});


app.post('/api/move', function (req, res) {
    
    var resultjson = {
    };
    
    if (req.body.move === undefined) {
        resultjson.msg="Nie ma move";
        res.json(resultjson);
    }
    
    if(req.body.move.length !== game.codeSize){
        resultjson.msg= ("Niepoprawna ilosc parametrów w move. Wymagana: " + game.codeSize);
        res.json(resultjson);   
    }
    var isSizeWrong = false;
    for(var i = 0; i<game.codeSize; i++){
        if (req.body.move[i] > game.maxColors ||  req.body.move[i] <= 0) {
            
            isSizeWrong = true;
            resultjson.msg += ("Liczba " + req.body.move[i] + " jest poza zakresem. \n");
        }
    }
    console.log(isSizeWrong);
    if (isSizeWrong) { res.json(resultjson); }

    
    //GRA!
    
    
    var isValid = true;
    var code = game.secret;

    
    var result = _.map(req.body.move , function (el) {
        var elem = NaN;
        //if(regexLiczba(/^\d+$/,el)){
            elem = parseInt(el,10);
        //}
        if (isNaN(elem)) {
            isValid = false;
            return "Error, number expected";
        } 
        else {
            return "Liczba: " + el;
        }
    });    
    
    
    
    if (isValid){ 
        var guesstab =  _.map(req.body.move, function(el) {
            console.log(el);
            return parseInt(el,10);
        });
        var obj = mark(code, guesstab);
        if(obj.black === 5) {
        res.end("Black:"+ obj.black +" White:"+ obj.white+" YOU WIN!");
    }
        res.end("Black:"+ obj.black +" White:"+ obj.white);
        
    }
    else{
        res.end("NIEPOPRAWNE PARAMETRY " + result[0] + " | " + result[1] + " | " + result[2] + " | " + result[3]           +" | "+ result[4] ); 
    }
    
    
    res.json(resultjson);
    
    
});
/*

app.post('/api/move', function (req, res) {
    {
        move [1,2,6,3,5]   
    }
}
*/
/*
	"maxMoves": 10,
    "maxColors": 9,
    "codeSize": 5

*/



app.get('/api/move/:move1/:move2/:move3/:move4/:move5', function (req, res) {
    //console.dir(req.params);
    var isValid = true;
    var code = [3,7,6,8,6];
  
    /*
    var obj = mark([4,2,4,1,7],[4,4,2,4,1]);
console.log("Czarne:", obj.black, "Białe:", obj.white);
*/
    
    
    var result = _.map(req.params, function (el) {
        var elem = NaN;
        //if(regexLiczba(/^\d+$/,el)){
            elem = parseInt(el,10);
        //}
        if (isNaN(elem)) {
            isValid = false;
            return "Error, number expected";
        } 
        else {
            return "Liczba: " + el;
        }
    });    
    
    
    
    if (isValid){ 
        var guesstab =  _.map(req.params, function(el) {
            console.log(el);
            return parseInt(el,10);
        });
        var obj = mark(code, guesstab);
        if(obj.black === 5) {
        res.end("Black:"+ obj.black +" White:"+ obj.white+" YOU WIN!");
    }
        res.end("Black:"+ obj.black +" White:"+ obj.white);
        
    }
    else{
        res.end("NIEPOPRAWNE PARAMETRY " + result[0] + " | " + result[1] + " | " + result[2] + " | " + result[3]           +" | "+ result[4] ); 
    }
    
    
/*res.end("Dostalismy: " + req.params.move1 + " " + req.params.move2 + " " + req.params.move3 + " " + req.params.move4 +" "+ req.params.move5 ); */
});

//POST /api/newgame
// { "maxMoves": n, "maxColors" : m (9), "codeSize": 5}  ilosc ruchow, najwyzsza liczba do zgadywania oraz wielkosc kodu
                               
app.listen(8888, function () {
    console.log("Serwer działa na porcie 8888 ");
});